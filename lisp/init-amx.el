(require 'amx)

(global-set-key (kbd "M-x") (lambda ()
                             (interactive)
                             (or (boundp 'amx-cache)
                                 (amx-initialize))
                             (global-set-key (kbd "M-x") 'amx)
                             (amx)))

(provide 'init-amx)
