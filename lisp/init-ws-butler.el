(autoload 'ws-butler-mode "ws-butler" t nil)

(add-hook 'c-mode-hook #'(lambda ()
                           (ws-butler-mode)
                           (diminish 'ws-butler-mode)))

(provide 'init-ws-butler)
