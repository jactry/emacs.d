(autoload 'counsel-rg "counsel.el" t)
(autoload 'counsel-grep "counsel.el" t)

(defun counsel-rg-current-directory ()
  (interactive)
  (counsel-rg nil default-directory))

(global-set-key (kbd "C-c i") 'counsel-rg)
(global-set-key (kbd "C-c e") 'counsel-rg-current-directory)
(global-set-key (kbd "C-c q") 'counsel-grep)

(provide 'init-counsel)
