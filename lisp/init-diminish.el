(require 'diminish)

(diminish 'guru-mode)
(diminish 'eldoc-mode)
(diminish 'abbrev-mode)
(diminish 'ahs-edit-mode)
(diminish 'git-gutter-mode)
(diminish 'smartparens-mode)
(diminish 'global-whitespace-mode)
(diminish 'auto-highlight-symbol-mode)

(provide 'init-diminish)
