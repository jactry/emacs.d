(autoload 'company-mode "company.el" t)
(autoload 'company-box-mode "company-box.el")
(autoload 'company-tabnine "company-tabnine.el" t)

(declare-function all-the-icons-faicon 'all-the-icons)
(declare-function all-the-icons-material 'all-the-icons)
(declare-function all-the-icons-octicon 'all-the-icons)
(setq company-box-icons-all-the-icons
      `((Unknown . ,(all-the-icons-material "find_in_page" :height 0.8 :v-adjust -0.15))
        (Text . ,(all-the-icons-faicon "text-width" :height 0.8 :v-adjust -0.02))
        (Method . ,(all-the-icons-faicon "cube" :height 0.8 :v-adjust -0.02 :face 'all-the-icons-purple))
        (Function . ,(all-the-icons-faicon "cube" :height 0.8 :v-adjust -0.02 :face 'all-the-icons-purple))
        (Constructor . ,(all-the-icons-faicon "cube" :height 0.8 :v-adjust -0.02 :face 'all-the-icons-purple))
        (Field . ,(all-the-icons-octicon "tag" :height 0.85 :v-adjust 0 :face 'all-the-icons-lblue))
        (Variable . ,(all-the-icons-octicon "tag" :height 0.85 :v-adjust 0 :face 'all-the-icons-lblue))
        (Class . ,(all-the-icons-material "settings_input_component" :height 0.8 :v-adjust -0.15 :face 'all-the-icons-orange))
        (Interface . ,(all-the-icons-material "share" :height 0.8 :v-adjust -0.15 :face 'all-the-icons-lblue))
        (Module . ,(all-the-icons-material "view_module" :height 0.8 :v-adjust -0.15 :face 'all-the-icons-lblue))
        (Property . ,(all-the-icons-faicon "wrench" :height 0.8 :v-adjust -0.02))
        (Unit . ,(all-the-icons-material "settings_system_daydream" :height 0.8 :v-adjust -0.15))
        (Value . ,(all-the-icons-material "format_align_right" :height 0.8 :v-adjust -0.15 :face 'all-the-icons-lblue))
        (Enum . ,(all-the-icons-material "storage" :height 0.8 :v-adjust -0.15 :face 'all-the-icons-orange))
        (Keyword . ,(all-the-icons-material "filter_center_focus" :height 0.8 :v-adjust -0.15))
        (Snippet . ,(all-the-icons-material "format_align_center" :height 0.8 :v-adjust -0.15))
        (Color . ,(all-the-icons-material "palette" :height 0.8 :v-adjust -0.15))
        (File . ,(all-the-icons-faicon "file-o" :height 0.8 :v-adjust -0.02))
        (Reference . ,(all-the-icons-material "collections_bookmark" :height 0.8 :v-adjust -0.15))
        (Folder . ,(all-the-icons-faicon "folder-open" :height 0.8 :v-adjust -0.02))
        (EnumMember . ,(all-the-icons-material "format_align_right" :height 0.8 :v-adjust -0.15))
        (Constant . ,(all-the-icons-faicon "square-o" :height 0.8 :v-adjust -0.1))
        (Struct . ,(all-the-icons-material "settings_input_component" :height 0.8 :v-adjust -0.15 :face 'all-the-icons-orange))
        (Event . ,(all-the-icons-octicon "zap" :height 0.8 :v-adjust 0 :face 'all-the-icons-orange))
        (Operator . ,(all-the-icons-material "control_point" :height 0.8 :v-adjust -0.15))
        (TypeParameter . ,(all-the-icons-faicon "arrows" :height 0.8 :v-adjust -0.02))
        (Template . ,(all-the-icons-material "format_align_left" :height 0.8 :v-adjust -0.15)))
      company-box-icons-alist 'company-box-icons-all-the-icons)

(add-hook 'company-mode-hook #'(lambda ()
                                 (company-box-mode)
                                 (diminish 'company-box-mode)))

(setq company-minimum-prefix-length 1
      company-tooltip-limit 8
      company-idle-delay 0.1
      company-show-numbers t)

(defun setup-company-backends-for-c-mode ()
  (setq company-clang-arguments '("-I../../include"
                                  "-I../../../wine/include"
                                  "-I../../../include"
                                  "-I../../../../wine/include"
                                  "-I../../../build/wine.win32/include"
                                  "-I../../../../build/wine.win32/include"
                                  "-I../../../build/wine.win64/include"
                                  "-I../../../../build/wine.win64/include"))
  (setq company-backends '((company-clang :with company-tabnine :separate)
                           company-files company-capf)))
(add-hook 'c-mode-hook #'(lambda ()
                           (setup-company-backends-for-c-mode)
                           (company-mode)
                           ;; Remove duplications
                           (add-to-list 'company-transformers #'delete-dups)))

(defun setup-company-backends-for-emacs-lisp-mode ()
  (setq company-backends '(company-capf)))
(add-hook 'emacs-lisp-mode-hook #'(lambda ()
                                    (setup-company-backends-for-emacs-lisp-mode)
                                    (company-mode)))

(with-eval-after-load 'company
  (dolist (map (list company-active-map company-search-map))
    (define-key map (kbd "C-n") nil)
    (define-key map (kbd "C-p") nil)
    (define-key map (kbd "M-n") #'company-select-next)
    (define-key map (kbd "M-p") #'company-select-previous)))

(provide 'init-company)
