(require 'colorful-mode)

(global-colorful-mode)
(setq colorful-use-prefix t
      colorful-prefix-string "● ")

(provide 'init-colorful-mode)
