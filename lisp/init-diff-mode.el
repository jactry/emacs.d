(add-to-list 'magic-mode-alist
             '("\\(.\\|\n\\)*diff --git a" . diff-mode))
(add-hook 'diff-mode-hook
          (lambda()
            (local-unset-key (kbd "M-n"))
            (local-unset-key (kbd "M-p"))
            (define-key diff-mode-map (kbd "C-c n") 'diff-file-next)
            (define-key diff-mode-map (kbd "C-c p") 'diff-file-prev)))

(provide 'init-diff-mode)
