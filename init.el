(let ((default-directory  "~/.emacs.d/lisp/"))
  (normal-top-level-add-to-load-path '("."))
  (normal-top-level-add-subdirs-to-load-path))

(require 'init-base)

(require 'init-interface)

(require 'init-switch-window)

(require 'init-ido)

(require 'init-guru-mode)

(require 'init-electric-pair-mode)

(require 'init-auto-highlight-symbol)

(require 'init-whitespace)

(require 'init-highlight-indent-guides)

(require 'init-fill-column-indicator)

(require 'init-colorful-mode)

(require 'init-rainbow-delimiters)

(require 'init-rainbow-identifiers)

(require 'init-all-the-icons)

;; company-box must be loaded after all-the-icons
(require 'init-company)

(require 'init-powerline)

;; centaur-tabs must be loaded after all-the-icons and powerline
(require 'init-centaur-tabs)

(require 'switch-window)

(require 'init-git-gutter)

(require 'init-markdown-mode)

(require 'init-cedet)

(require 'init-amx)

(require 'init-counsel)

(require 'init-diff-mode)

(require 'init-ediff)

(require 'init-ws-butler)

(require 'init-goto-line-preview)

(require 'init-c-mode)

(require 'init-vc)

(require 'init-hs-minor-mode)

(require 'init-cython-mode)

(require 'init-csharp-mode)

(require 'init-go-mode)

(require 'init-llvm-mode)

(require 'init-rust-mode)

(require 'init-z3-mode)

(require 'init-diminish)
